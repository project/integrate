<?php

/**
 * @file
 * Administrative page callbacks for the integrate module.
 */
/**
 * Constants for module name 
 */
 define("INTEGRATE_MODULE_NAME","token");
 
/**
 * Implements hook_admin_settings().
 */
function integrate_admin_settings_form($form, &$form_state) {
  $form['integrate'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Remove Generator
  $form['integrate']['integrate_meta_generator'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove generator Meta Tag'),
    '#description' => t('If enabled this module will remove Generator Tag from your site.'),
    '#default_value' => variable_get('integrate_meta_generator', 0),
  );
   $form['integrate']['copyright'] = array(
    '#type' => 'checkbox',
    '#title' => t('Replace Copyright year'),
    '#description' => t('If enabled copyright token will be available for Replace Copyright year .'),
    '#default_value' => variable_get('copyright', 0),
  );
  $form['integrate']['lastupdated'] = array(
    '#type' => 'checkbox',
    '#title' => t('Replaces with last update date on the site'),
    '#description' => t('If enabled copyright token will be available for Replacing with last update date on the site .'),
    '#default_value' => variable_get('lastupdated', 0),
  );
  return system_settings_form($form);
}
function integrate_admin_settings_form_validate($form, &$form_state) {  
  if($form_state['values']['copyright'] || $form_state['values']['lastupdated'] != FALSE) {
	//Check weather token module is enable or not 
	if(!module_exists(INTEGRATE_MODULE_NAME)) {
	  form_set_error("copyright", t("Please enable token module before using it"));
	  return FALSE;
	}
  }
  if($form_state['values']['integrate_meta_generator'] != FALSE) {
    cache_clear_all();
  } 
}
